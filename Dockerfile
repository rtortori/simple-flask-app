# Use a lightweight base image
FROM rtortori/alpine-python3

# Set the working directory to /
WORKDIR /

# Copy required configuration files
ADD requirements.txt sample_pyapp.py /

# Optionally define proxy environment
#ENV http_proxy host:port
#ENV https_proxy host:port

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Run it
CMD ["python", "/sample_pyapp.py"]

# Expose a port
EXPOSE 5000
