# Import required modules
from flask import request, Flask, jsonify, abort, make_response
import json

# Define Flask app name
sample_api = Flask(__name__)

# Create an error handler
@sample_api.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 404}), 404)

# Sample API, it will return a simple JSON response
@sample_api.route('/api/v1/sample', methods=['GET'])
def sample():
    try:
        payload = {
            "a": "b"
        }
    except Exception:
        print('Sorry')
        abort(404)
    return jsonify(payload)

# Run API server as long as the program is running
if __name__ == '__main__':
    sample_api.run(host='0.0.0.0', debug=True)
